#!/usr/bin/python
# not compatible with python 3
__author__		= "Sander Granneman"
__copyright__	= "Copyright 2014"
__version__		= "0.0.1"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


import sys
from optparse import *
from pyChemMod.Methods import getfilename

help="""

pyChemModMergeSgrFiles.py version %s

To run this program, simply type pyChemModMergeSgrFiles.py in the command line, followed by the names of the
files that you want to merge. By default, the program prints the results to the standard output.

For example:

pyChemModMergeSgrFiles.py control1 control2 control2 treated1 treated2 treated3 > merged_data.txt
""" % __version__

def main():
	parser = OptionParser(help, version="%s" % __version__)
	(options, args) = parser.parse_args()
	
	files = [open(i,"r") for i in sys.argv[1:]]
	filenames = [getfilename(i) for i in sys.argv[1:]]
	sys.stdout.write("# chromosome\tposition\t%s\n" % "\t".join(filenames))
	while True:
		lines = []
		try:
			lines = [i.next().strip().split() for i in files]
		except StopIteration:
			break
		assert len(set([i[0] for i in lines])) == 1, "The chromosome names in the sgr files are not properly aligned: %s. Are the column lenghts uneven?\n" % ",".join([i[0] for i in lines])
		assert len(set([i[1] for i in lines])) == 1, "The chromosome positions in the sgr files are not properly aligned. Are the column lenghts uneven?\n" % ",".join([i[1] for i in lines])
		string = "%s\t" % "\t".join(lines[0][:3])							# first three lines of the first file are required
		string += "%s\n" % "\t".join([i[2] for i in lines[1:]])				# then the third column of each consecutive file
		sys.stdout.write(string)

if __name__ == "__main__":
	main()