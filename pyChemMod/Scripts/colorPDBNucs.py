#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "1.1.0"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


"""
Script to color the residues in the 80S crystal structure.
"""
import os
from pymol import cmd
import numpy as np
import matplotlib.cm as cm
import matplotlib.colors as colors
from matplotlib import *
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['font.sans-serif'] = ["Arial"]

def Normalize(data,minvalue=None,maxvalue=None):
	data = np.array(data)
	totalnumbers = len(data)+1
	minimum = np.min(data)
	maximum = np.max(data)+abs(minimum)
	if minvalue:
		if minvalue < minimum:
			data = np.append(data,float(minimum))
			data[data<minvalue] = minvalue
	if maxvalue:
		if maxvalue > maximum:
			data = np.append(data,float(maximum))
			data[data>maxvalue] = maxvalue
	return [((i+abs(minimum))/maximum) for i in data[:totalnumbers]]

def getNucleotideColorInfo(inputfile,column=2,colormap="jet",discrete=False,colorbar=False,minvalue=None,maxvalue=None,filter=[]):
	""" processes the file containing the nucleotide positions and the relative reactivity"""
	if discrete:
		my_cmap = colors.ListedColormap(['#00a0b0', 'b', '#00ff00', '#ffcc00', '#ff6600', 'r']) # black,blue,lime,yellow,orange,red
	else:
		cdict = cm.datad[colormap]
		my_cmap = colors.LinearSegmentedColormap('my_colormap',cdict,256)
	data = pd.read_table(inputfile,index_col=0)
	column -= 2
	if column < 0: column = 0
	selecteddata = data.ix[:,column]
	normdata = Normalize(selecteddata)
	colorvalues = my_cmap(normdata)		# still needs to be corrected for zeros
	for i in data.index:
		if data.ix[i,column] == 0.0:  # if the value is zero, color the nucleotides black.
			colorvalues[i-1] = [0.0, 0.0, 0.0, 1]
		if filter:
			min,max = filter			# color nucleotides with values in a specified range black
			if data.ix[i,column] > min and data.ix[i,column] < max:
				colorvalues[i-1] = [0.0, 0.0, 0.0, 1]
	if colorbar:
		react = np.array(selecteddata)
		image = plt.scatter(np.arange(1,totaldatapoints+1),react,c=react,cmap=my_cmap,vmin=minvalue,vmax=maxvalue)
		plt.colorbar(image)
		plt.savefig("colorbar.pdf")

	return colorvalues

def alterColors(inputfile,object="3U5B",chain="2",name="mods",color="red",score=1):
	"""adds a specific color for a specific value in the input file"""
	data = open(inputfile,"r").readlines()
	score = float(score)
	residues = list()
	for line in data:
		if line.startswith("#"): continue
		Fld = line.strip().split()
		pos = int(Fld[0])
		react = float(Fld[1])
		if react >= score:
			residues.append(str(pos))
	resi = "+".join(residues)
	cmd.select("(%s and chain %s and resi %s)" % (object,chain,resi))
	cmd.select(name,"sele")
	if color:
		cmd.color(color,name)

def colorGradient(inputfile,object="3U5B",chain="2",name="mods",colormap="Spectral",discrete=False):
	"""generates a color gradient in the pdb object"""
	nucinfo = getNucleotideColorInfo(inputfile,colormap=colormap,discrete=discrete)
	for pos,i in enumerate(nucinfo,start=1):
		newcolor = "col_%s" % pos
		cmd.set_color(newcolor,list(i[:-1]))
		cmd.color(newcolor,"(%s and chain %s and resi %s)" % (object,chain,pos))

cmd.extend("getNucleotideColorInfo",getNucleotideColorInfo)
cmd.extend("alterColors",alterColors)
cmd.extend("colorGradient",colorGradient)
#cmd.fetch("3U5B")
#cmd.show("cartoon")
#cmd.color("wheat")
#cmd.set("cartoon_ring_finder",3) # (or 2 or 3 or 4)
#cmd.set("cartoon_nucleic_acid_mode",3)	 # (or 1 or 2 or 3 or 4)
#cmd.set("cartoon_ring_mode",3)

