#!/usr/bin/python

__author__      = "Sander Granneman"
__copyright__   = "Copyright 2019"
__version__     = "0.0.2"
__credits__     = ["Sander Granneman"]
__maintainer__  = "Sander Granneman"
__email__       = "sgrannem@staffmail.ed.ac.uk"
__status__      = "beta"

import sys
import re
import math
import numpy as np
import matplotlib.cm as cm
import matplotlib.colors as colors
from optparse import *
from collections import defaultdict
from pyChemMod.Methods import getfilename  

def getNucleotideColorInfo(inputfile,colormap="copper",discrete=True):
	""" processes the file containing the nucleotide positions and the relative reactivity"""
	if discrete:
		my_cmap = colors.ListedColormap(['w', 'b', '#00ff00', '#ffcc00', '#ff6600', 'r']) # black,blue,lime,yellow,orange,red
	else:
		cdict = cm.datad[colormap]
		my_cmap = colors.LinearSegmentedColormap('my_colormap',cdict,256)
	react = list()
	for i in open(inputfile,"r").readlines():
		if i.startswith("#"):
			continue
		else:
			react.append(float(i.strip().split("\t")[1]))
	maximum = float(max(react))
	data = np.array([i/maximum for i in react])
	colorvalues = my_cmap(data)
	return colorvalues
	
def rgbtohex(rgb):
	""" convert an (r, g, b) tuple to #RRGGBB """
	r,g,b = [int(i*255) for i in rgb[:3]]
	hexcolor = '#%02x%02x%x' % (r,g,b)
	return hexcolor

def colorVarnaFile(inputfile,nucinfo,outfile=None):
	""" function for coloring nucleotides in a postscript file """
	if not outfile:
		outfile = "%s.VARNA" % getfilename(inputfile)
	out = sys.stdout
	if outfile: out = open(outfile,"w")	   
	outercolor = re.compile("outline=\"(#[A-Z0-9]+)\"")
	with open(inputfile,"r") as varnafile:
		count = 0
		for line in varnafile:
			if re.search("<basestyle outline=",line):
				colorstring = rgbtohex(nucinfo[count])
				line = line.replace(outercolor.findall(line)[0],colorstring)
				out.write(line)
				count += 1
			else:
				out.write(line)
				
def main():
	parser = OptionParser(usage="usage: %prog [options] -p postscriptfile.ps -d datafile.txt -o coloredpostscriptfile.ps", version="%s" % __version__)
	parser.add_option("-f", "--varnafile", dest="file",help="Type the path of the postscript input file", metavar="myvarnafile.VARNA")
	parser.add_option("-d", "--datafile", dest="data",help="Type the path of the data file. This should contain two columns, one with the nucleotide positions, the other with the color coding index", metavar="indexfile.txt")
	parser.add_option("-o", "--outfile", dest="outfile",help="Type the name of your output file. Don't forget to add the .VARNA extension", metavar="mycoloredvarnafile.VARNA",default=None)
	(options, args) = parser.parse_args()
	nucinfo = getNucleotideColorInfo(options.data)
	colorVarnaFile(options.file,nucinfo,options.outfile)
				
if __name__ == "__main__":
	main()
