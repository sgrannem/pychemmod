#!/usr/bin/python
# not compatible with python 3
__author__		= "Sander Granneman"
__copyright__	= "Copyright 2014"
__version__		= "0.0.1"
__credits__		= ["Sander Granneman","Christel Sirocchi"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


import sys
from optparse import *
from pyChemMod.Methods import getfilename

help="""

pyChemModMergeReactivityFiles.py version %s

To run this program, simply type pyChemModMergeReactivityFiles.py in the command line, followed by the names of the
files that you want to merge. By default, the program prints the results to the standard output.

For example:

pyChemModMergeReactivityFiles.py SHAPE_reactivities1.txt SHAPE_reactivities2.txt SHAPE_reactivities3.txt > merged_Reactivities.txt
""" % __version__

def main():
	parser = OptionParser(help, version="%s" % __version__)
	(options, args) = parser.parse_args()
	
	files = [open(i,"r") for i in sys.argv[1:]]
	filenames = [getfilename(i) for i in sys.argv[1:]]
	sys.stdout.write("# position\t%s\n" % "\t".join(filenames))
	while True:
		lines = []
		try:
			lines = [i.next().strip().split() for i in files]
		except StopIteration:
			break
		string = "%s\t" % "\t".join(lines[0][:2])							# first two lines of the first file are required
		string += "%s\n" % "\t".join([i[1] for i in lines[1:]])				# then the second column of each consecutive file
		sys.stdout.write(string)

if __name__ == "__main__":
	main()