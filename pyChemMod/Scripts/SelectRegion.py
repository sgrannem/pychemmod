#!/usr/bin/python
# not compatible with python 3
__author__	   = "Sander Granneman"
__copyright__  = "Copyright 2014"
__version__	   = "0.0.4"
__credits__	   = ["Sander Granneman"]
__maintainer__ = "Sander Granneman"
__email__	   = "sgrannem@staffmail.ed.ac.uk"
__status__	   = "beta"

import sys
import pandas as pd
from optparse import *

def selectRegions(infile,outfile,chromosome,strand,coordinates=[]):
	data = pd.read_csv(infile,header=0,index_col=None,sep="\t")
	start,end = int(coordinates[0]),int(coordinates[1])
	columns = data.columns
	region = data[(data[columns[0]]==chromosome) & (data[columns[1]]>=start) & (data[columns[1]]<=end)]
	if strand == "-":
		region = region.ix[::-1]
	region.to_csv(outfile,sep="\t",index=False,header=False)

def main():
	parser = OptionParser(usage="usage: %prog [options] -f infile -o outfile -d 10", version="%s" % __version__)
	parser.add_option("-f",dest="infile",metavar="datafile",help="type the path to the datafile.",default=None)
	parser.add_option("-o","--outfile",dest="outfile",help="type the name and path of the file you want to write the output to. Default is standard output",default=None)
	parser.add_option("-s","--strand",dest="strand",help="type the stand ('-' or '+') on which the gene is located",default=None)
	parser.add_option("-c","--chromosome",dest="chromosome",help="type the name of the chromosome where your gene is located",default=None)
	parser.add_option("--coord","--coordinates",type=int,nargs=2,dest="coord",help="provide the coordinates that you want to look at. For example: -c 1800 2100")
	(options, args) = parser.parse_args()
	out	 = sys.stdout
	data = options.infile
	if not options.strand:      parser.error("no strand information provided")
	if not options.chromosome:  parser.error("no chromosome name provided")
	if not options.coord:       parser.error("no chromosome coordinates provided")
	if options.outfile: out = open(options.outfile,"w")
	if not options.coord:
		parser.error("no coordinates provided. Please use the -c flag to enter two coordinates")
	selectRegions(data,out,options.chromosome,options.strand,options.coord)

if __name__ == "__main__":
	main()