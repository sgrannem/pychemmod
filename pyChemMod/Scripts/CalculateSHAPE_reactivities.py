#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.4"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"

import sys
import pandas as pd
import numpy as np
from math import *
from optparse import *
np.seterr(all="ignore")

def toNumpyArray(data,ignorecolumns=2):
	""" loads the text files and converts it to numpy arrays. Removes the first two columns and returns	 """
	return np.array(np.loadtxt(data,comments="#",delimiter="\t",dtype=str)[:,ignorecolumns:],dtype=float)

def printModResults(modnucs,stderr=None,outfile=None):
	""" prints the SHAPE reactivity values to an output file with two columns """
	if not outfile: out = sys.stdout
	else: out = outfile
	for i in modnucs:
		try:
			if stderr:
				out.write("%s\t%s\t%s\n" % (i+1, modnucs[i+1],stderr[i+1]))
			else:
				out.write("%s\t%s\n" % (i+1,modnucs[i+1]))
		except KeyError:
			pass
			
def calculateSHAPEreactivities(controldropoffs,treateddropoffs,removezeros=False):
	""" Normalizes the drop-off counts using Tang et al 2015 approach """
	transcriptlength = float(controldropoffs.shape[0])
	assert transcriptlength == treateddropoffs.shape[0], "Coverage data and drop-off data are not of the same length!\n"
	logcontroldropoffs = np.log(controldropoffs + 1)									# log transform the data. 1 is added to also be able to include the zeros in the calculations
	logtreateddropoffs = np.log(treateddropoffs + 1)
	controlaverage = sum(logcontroldropoffs)/transcriptlength							# calculate the average drop-offs per nucleotide
	treatedaverage = sum(logtreateddropoffs)/transcriptlength
	reactivitiescontrol = logcontroldropoffs/controlaverage								# normalize the counts by dividing them to the mean
	reactivitiestreated = logtreateddropoffs/treatedaverage
	rawreactivities = reactivitiestreated - reactivitiescontrol							# calculate reactivities by subtracting modified from unmodified data
	if not removezeros:	
		rawreactivities[rawreactivities<0] = 0
	return rawreactivities
	
def normalizeSHAPEvalues(data):
	""" performs a 2-8% normalisation on the SHAPE reactivity. This is a method used by the Weeks lab to get SHAPE reacitivies between 0 and ~2"""
	sorteddata	 = np.sort(data)
	twopercent	 = int(round(len(sorteddata)*.02))
	eightpercent = int(round(len(sorteddata)*.08))
	filtereddata = sorteddata[:(len(data)-twopercent+1)]
	summeddata = np.sum(filtereddata[-eightpercent:])
	normfactor = summeddata/eightpercent
	shapereactivities = np.array([i/normfactor if i > 0.0 else i for i in data])
	return shapereactivities
	
def estimateStandardErrors(controlcoverage,treatedcoverage,controldropoffs,treateddropoffs):
	""" estimates the standard error for the SHAPE reactivity, assuming drop-off counts are Poisson distributed """
	dropoffratescontrols = controldropoffs/controlcoverage
	dropoffratestreated  = treateddropoffs/treatedcoverage
	standarderrorcontrols = np.sqrt(dropoffratescontrols)/np.sqrt(controlcoverage)
	standarderrortreated  = np.sqrt(dropoffratestreated)/np.sqrt(treatedcoverage)
	finalstandarderrors = np.sqrt(np.square(standarderrorcontrols)+np.square(standarderrortreated))
	return finalstandarderrors

def findGoodCoveragePositions(coverage,dropoffs,mincoverage=1,mindropoffs=1):
	""" returns an index for positions where both the coverage counts and drop-off counts are above set thresholds """
	transcriptlength = dropoffs.shape[0]
	assert transcriptlength == coverage.shape[0], "Coverage data and drop-off data are not of the same length!\n"
	allpositions = np.arange(transcriptlength)
	coverage_index = set(allpositions[(coverage >= mincoverage).all(axis=1)])
	dropoffs_index = set(allpositions[(dropoffs >= mindropoffs).all(axis=1)])
	both = list(coverage_index & dropoffs_index)
	return allpositions[both]

def main():
	parser = OptionParser(usage="usage: %prog [options] -f dropoffcountdata -c coveragedata --replicates 3 3 -o SHAPE_reactivities.txt", version="%s" % __version__)
	parser.add_option("-c", "--coverage",dest="coverage",help="Name of the tab delimited input file containing the coverage data for the region of interest", metavar="myfavdata_coverge.txt",default=None)
	parser.add_option("-d", "--dropoffcounts",dest="dropoffcounts",help="Name of the tab delimited input file containing the drop-off counts for the region of interest", metavar="myfavdata_dropoffs.txt",default=None)
	parser.add_option("-o", "--outfile",dest="outfile",help="Provide an output file name. Default is standard output",default=None)
	parser.add_option("--replicates",dest="replicates",nargs=2,type="int",help="Provide the number of replicates for control and modified samples. The program expects the control samples to be in the first columns of the input file. Expects two values")
	parser.add_option("--mincoverage",dest="mincoverage",type="float",help="to set a coverage threshold. Default is a minimum of 1 read for each nucleotide",default=1)
	parser.add_option("--mindropoffs",dest="mindropoffs",type="float",help="to set a drop-off count threshold. Default is a minimum of 1 drop-off for each nucleotide",default=1)
	parser.add_option("--maxreactivity",dest="maxreactivity",type="float",help="to set a cap for the maximum reactivity for a nucleotide.",default=100)
	parser.add_option("--sum",dest="sum",action="store_true",help="to sum the dropoff counts from each replicate rather than taking the mean. Default = False",default=False)
	parser.add_option("--meanshape",dest="meanshape",action="store_true",help="to calculate SHAPE reactivities for each experiment individiually and then average the results. Returns averages and standard deviations",default=False)
	parser.add_option("--raw",dest="raw",action="store_true",help="to report the raw SHAPE reactivities without any removal of negative values or normalization. Default is OFF",default=False)
	parser.add_option("--normalize",dest="normalize",action="store_true",help="to normalize the SHAPE reactivities using the Weeks 2-8% normalization method. All the reactivities will lie between 0 and ~2",default=False)
	parser.add_option("--stderr",dest="stderr",action="store_true",help="to calculate the standard error of the SHAPE reactivities for each nucleotide. Default = False",default=False)
	(options, args) = parser.parse_args()
	if not options.replicates:
		parser.error("\nPlease use the --replicates flag to indicate the number of replicates for the control and modified samples\n")
	if options.sum and options.meanshape:
		parser.error("\n--sum and --meanshape options can not be used simultaneously\n")
	if options.meanshape and options.stderr:
		parser.error("\n--meanshape and --stderr flags can not be used simultaneously\n")
	if options.normalize and options.raw:
		parser.error("\n--normalize and --raw flags flags can not be used simultaneously\n")
	coverage = toNumpyArray(options.coverage)
	dropoffs = toNumpyArray(options.dropoffcounts)
	transcriptlength = coverage.shape[0]
	assert transcriptlength == dropoffs.shape[0], "\nCoverage data and drop-off data are not of the same length!\n"

	### removing positions with low drop-off count or low coverage
	index = findGoodCoveragePositions(coverage,dropoffs,mincoverage=options.mincoverage,mindropoffs=options.mindropoffs)
	filtered = dropoffs[index]
	coveragefiltered = coverage[index]

	if not filtered.any():
		sys.stderr.write("\nNo drop-off counts higher than %s in file %s\n" % (options.mindropoffs,options.dropoffcounts))
	###

	outfile = sys.stdout
	if options.outfile:
		outfile = open(options.outfile,"w")

	### splitting the array in to control and treated data.
	controlreps,treatedreps = options.replicates
	controldropoffs = filtered[:,0:controlreps]
	treateddropoffs = filtered[:,controlreps:controlreps+treatedreps]
	controlcoverage = coveragefiltered[:,0:controlreps]
	treatedcoverage = coveragefiltered[:,controlreps:controlreps+treatedreps]
	###

	standarderrors = np.array(0)

	if options.sum:														# sum the drop-off counts of different replicates
		controldropoffs = np.sum(controldropoffs,axis=1)
		treateddropoffs = np.sum(treateddropoffs,axis=1)
		controlcoverage = np.sum(controlcoverage,axis=1)
		treatedcoverage = np.sum(treatedcoverage,axis=1)
		reactivities = calculateSHAPEreactivities(controldropoffs,treateddropoffs,removezeros=options.raw)
	elif options.meanshape:												# average the shape reactivities of each replicate
		allreactivities = list()
		for i in range(controlreps):
			controls = controldropoffs[:,i]
			treated	 = treateddropoffs[:,i]
			normalized = logNormalizeDropOffcounts(controls,treated)
			indivreactivities = calculateSHAPEreactivities(normalized,removezeros=options.raw)
			allreactivities.append(indivreactivities)
		allreactivities = np.array(allreactivities).T
		reactivities = np.mean(allreactivities,axis=1)
	else:																# average the drop-off counts of different replicates
		controldropoffs = np.mean(controldropoffs,axis=1)
		treateddropoffs	= np.mean(treateddropoffs,axis=1)
		controlcoverage = np.mean(controlcoverage,axis=1)
		treatedcoverage = np.mean(treatedcoverage,axis=1)
		reactivities = calculateSHAPEreactivities(controldropoffs,treateddropoffs,removezeros=options.raw)

	### in case you want to also include the estimated standard errors for each nucleotide ###

	if options.stderr:
		standarderrors = estimateStandardErrors(controlcoverage, treatedcoverage, controldropoffs, treateddropoffs)
	###

	### in case you want to perform a 2-8% normalization or just cap the raw SHAPE reactivities at a specific value

	if options.normalize:
		reactivities = normalizeSHAPEvalues(reactivities)
	else:
		reactivities[reactivities > options.maxreactivity] = options.maxreactivity

	###
	reactivitydict = dict(list(zip(list(range(transcriptlength)),[-999]*transcriptlength))) 	# all values are -500 (low coverage/low drop-offs) until SHAPE reactivities have been calculated
	reactivitydict.update(dict(list(zip(index,reactivities))))									# then the dictionary is updated with the SHAPE reactivities from positions with good coverage and good number of drop-offs
	if options.stderr:
		stderrdict = dict(list(zip(list(range(transcriptlength)),[-999]*transcriptlength)))
		stderrdict.update(dict(list(zip(index,standarderrors))))
		printModResults(reactivitydict,stderr=stderrdict,outfile=outfile)
	else:
		printModResults(reactivitydict,stderr=None,outfile=outfile)

if __name__ == "__main__":
	main()	
