  #!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2020"
__version__		= "0.3.0"
__credits__		= ["Sander Granneman","Stuart Aitken","Guido Sanguinetti","Christel Sirocchi"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


import sys

try:
	from setuptools import setup
	from setuptools.command import easy_install
	sys.stdout.write("Python development and setuptools have been installed...\n")
except:
	sys.stderr.write("Python development and setuptools have not been installed on this machine\nPlease contact the admin of this computer to install these modules\n")
	exit()

setup(name='ChemModSeq Pipeline scripts',
	version=__version__,
	description='software for processing high throughput chemical modification data',
	author='Sander Granneman',
	author_email='sgrannem@staffmail.ed.ac.uk',
	url='http://sandergranneman.bio.ed.ac.uk/',
	packages=['pyChemMod'],
	install_requires=['pyCRAC >= 1.4', 'numpy >= 1.5.1','scipy','rpy2 >= 2.3','matplotlib','pandas'],
	scripts=[
					'pyChemMod/__init__.py',
					'pyChemMod/Scripts/__init__.py',
					'pyChemMod/Scripts/colorPDBNucs.py',
					'pyChemMod/Scripts/colorStructureFile.py',
					'pyChemMod/Scripts/colorVarnaFile.py',
					'pyChemMod/Scripts/colorPDBNucs.py',
					'pyChemMod/Scripts/SelectRegion.py',
					'pyChemMod/Scripts/MergeSgrFiles.py',
					'pyChemMod/Scripts/MergeReactivityFiles.py',
					'pyChemMod/Scripts/ChemModSeqPipeline.py',
					'pyChemMod/Scripts/CalculateSHAPE_reactivities.py',
					'pyChemMod/Scripts/CalculatePPVs.py',

					],
	classifiers=[   'Development Status :: 5 - Production/Stable',
					'Environment :: Console',
					'Intended Audience :: Education',
					'Intended Audience :: Developers',
					'Intended Audience :: Science/Research',
					'License :: Freeware',
					'Operating System :: MacOS :: MacOS X',
					'Operating System :: POSIX',
					'Programming Language :: Python :: 3.6',
					'Topic :: Scientific/Engineering :: Bio-Informatics',
					'Topic :: Software Development :: Libraries :: Application Frameworks'
				]
			)
